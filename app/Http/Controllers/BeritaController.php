<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Berita;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('berita.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg',
            'kategori_id' => 'required',
        ]);

        $namaThumbnail = time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('thumbnail'), $namaThumbnail);

        $berita = new Berita;

        $berita->judul = $request->judul;
        $berita->content = $request->content;
        $berita->thumbnail = $namaThumbnail;
        $berita->kategori_id = $request->kategori_id;

        $berita->save();

        Alert::success('Berhasil', 'Berita Berhasil Ditambahkan');
        return redirect('/berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = Berita::findOrFail($id);
        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $berita = Berita::findOrFail($id);
        return view('berita.edit', compact('berita', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'content' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg',
            'kategori_id' => 'required',
        ]);
    if ($request->has('thumbnail')){
        $berita = Berita::find($id);

        $path = "thumbnail/";
        File::delete($path . $berita->thumbnail);

        $namaThumbnail = time().'.'.$request->thumbnail->extension();

        $request->thumbnail->move(public_path('thumbnail'), $namaThumbnail);

        $berita->judul = $request->judul;
        $berita->content = $request->content;
        $berita->thumbnail = $namaThumbnail;
        $berita->kategori_id = $request->kategori_id;

        $berita->save();

        Alert::success('Berhasil', 'Berita Berhasil Diperbarui');
        return redirect('/berita');
    }else{
        $berita = Berita::find($id);
        $berita->judul = $request->judul;
        $berita->content = $request->content;
        $berita->kategori_id = $request->kategori_id;

        $berita->save();

        Alert::success('Berhasil', 'Berita Berhasil Diperbarui');
        return redirect('/berita');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::find($id);

            $path = "thumbnail/";
            File::delete($path . $berita->thumbnail);

            $berita->delete();

            Alert::warning('Warning', 'Berita Telah Dihapus');
            return redirect('/berita');
    }
}

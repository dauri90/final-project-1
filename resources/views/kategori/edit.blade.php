@extends('layout.master')

@section('judul')
Halaman Edit Kategori {{$kategori->nama}}
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/9q1viwtvenq5zd7y17rordii1sm1jb32o0g4hieixg6cmsmy/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')
<form action="/kategori/{{$kategori->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" value="{{$kategori->nama}}" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Deskripsi</label>
        <textarea name="deskripsi" class="form-control">{{$kategori->deskripsi}}</textarea>
      </div>
      @error('deskripsi')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection

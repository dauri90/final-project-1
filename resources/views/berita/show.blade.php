@extends('layout.master')

@section('judul')
Halaman Detail Berita ID Ke- {{$berita->id}}
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/9q1viwtvenq5zd7y17rordii1sm1jb32o0g4hieixg6cmsmy/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')
<div class="d-flex justify-content-center">
<img src="{{asset('thumbnail/'.$berita->thumbnail)}}" height="500" class="my-3">
</div>
<h4>{{$berita->judul}}</h4>
<p>{{$berita->content}}</p>
<p class="mb-5">Tanggal Dibuat: {{$berita->created_at}}</p>

<h1>Komentar</h1>

@foreach ($berita->komentar as $item)
    <div class="card">
        <div class="card-body bg-light">
          <small><b class="text-primary">{{$item->user->name}}</b></small>
          <p class="card-text text-secondary">{{$item->isi}}</p>
        </div>
    </div>
@endforeach

<form action="/komentar" method="POST">
    @csrf
    <div class="form-group">
        <label class="mt-5">Tulis Komentar</label>
        <input type="hidden" name="berita_id" value="{{$berita->id}}">
        <textarea name="isi" class="form-control"></textarea>
      </div>
    @error('isi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Komentari</button>
  </form>
<a href="/berita" class="btn btn-secondary mt-2">Kembali</a>
@endsection

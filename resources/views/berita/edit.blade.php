@extends('layout.master')

@section('judul')
Halaman Edit Berita
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/9q1viwtvenq5zd7y17rordii1sm1jb32o0g4hieixg6cmsmy/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')
<form action="/berita/{{$berita->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Judul</label>
      <input type="text" name="judul" value="{{$berita->judul}}" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Content Berita</label>
        <textarea name="content" class="form-control">{{$berita->content}}</textarea>
      </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      <div class="form-group">
        <label>Thumbnail</label>
        <input type="file" class="form-control" value="{{$berita->thumbnail}}" name="thumbnail">
      </div>
    @error('thumbnail')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Kategori</label>
        <select name="kategori_id" class="form-control">
            <option value="">--Pilih Kategori--</option>
            @foreach ($kategori as $item)
                @if ($item->id === $berita->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
      </div>
    @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-success btn-sm">Submit</button>
  </form>

@endsection

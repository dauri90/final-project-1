<nav id="sidebar">
    <div class="sidebar_blog_1">
       <div class="sidebar-header">
          <div class="logo_section">
             <a href="/"><img class="logo_icon img-responsive" src="{{asset('admin/images/logo/logopks.png')}}" alt="#" /></a>
          </div>
       </div>
       @auth
       <div class="sidebar_user_info">
          <div class="icon_setting"></div>
          <div class="user_profle_side">
             <div class="user_img"><img class="img-responsive" src="{{asset('admin/images/layout_img/default.png')}}" alt="#" /></div>
             <div class="user_info">
                <h6>{{ Auth::user()->name }}</h6>
                <p><span class="online_animation"></span> Online</p>
             </div>
          </div>
       </div>
    </div>
    @endauth
    <div class="sidebar_blog_2">
       <h4>Menu Utama</h4>
       <ul class="list-unstyled components">
          <li class="active">
             <a href="/"><i class="fa fa-dashboard orange_color"></i> <span>Dashboard</span></a>
          </li>
          @auth
          <li>
            <a href="#tables" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-table white_color2"></i> <span>Tables</span></a>
            <ul class="collapse list-unstyled" id="tables">
               <li><a href="/kategori">> <span>Kategori</span></a></li>
               <li><a href="/berita">> <span>Berita</span></a></li>
               <li><a href="/profile">> <span>Profile</span></a></li>
            </ul>
         </li>
         @endauth
       </ul>
    </div>
 </nav>

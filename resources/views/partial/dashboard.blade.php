<<div class="card">
    <div class="card-header">
      <h3 class="card-title">@yield('judul')</h3>
    </div>
</div>
<div class="card-body">
    @yield('content')
</div>
    <!-- footer -->
    <div class="container-fluid">
       <div class="footer">
          <p>Copyright © 2021 Designed by Muhammad Dauri. All rights reserved.<br><br>
             Distributed By: <a href="https://pks.id/">PKS PGK</a>
          </p>
       </div>
    </div>
 </div>

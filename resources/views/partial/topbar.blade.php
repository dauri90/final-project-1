<div class="topbar">
    <nav class="navbar navbar-expand-lg navbar-light">
       <div class="full">
          <button type="button" id="sidebarCollapse" class="sidebar_toggle"><i class="fa fa-bars"></i></button>
          <div class="logo_section">
             <a href="index.html"><img class="img-responsive" src="{{asset('admin/images/logo/logopks3.png')}}" alt="#" /></a>
          </div>

          <div class="right_topbar">
            <div class="icon_info">
                @guest
                <div>
                <a class="btn btn-success mr-3 mt-3" href="/login"><span>Login</span><i class="fa fa-sign-in"></i></a>
                </div>
                @endguest
                @auth
                <ul class="user_profile_dd">
                    <li>
                      <a class="dropdown-toggle" data-toggle="dropdown"><span class="name_user"><i class="fa fa-user">_</i>{{ Auth::user()->name }}</span></a>
                      <div class="dropdown-menu">
                         <a class="dropdown-item" href="/profile">
                            My Profile
                            </a>
                         <a class="dropdown-item nav-link" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="nav-icon fa fa-sign-out" aria-hidden="true"></i>
                             <span>Log Out</span>
                             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                             </form>
                        </a>
                      </div>
                   </li>
                </ul>
                @endauth
             </div>
          </div>
       </div>
    </nav>
 </div>

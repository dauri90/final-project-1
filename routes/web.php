<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/adminpanel', function () {
    return view('layout.awal');
});

Route::get('/', function () {
    return view('layout.awal');
});



Route::group(['middleware' => ['auth']], function () {

    //CRUD Kategori Query
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/{kategori_id}', 'KategoriController@show');
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
Route::put('/kategori/{kategori_id}', 'KategoriController@update');
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

//Update Profile
Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

//Komentar
Route::resource('komentar', 'KomentarController')->only([
    'index', 'store'
]);

});

//CRUD Film
Route::resource('berita', 'BeritaController');

Auth::routes();
